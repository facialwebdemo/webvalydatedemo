/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bleubricks;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.util.encoders.Base64;
//import org.apache.tomcat.util.codec.binary.Base64;

import org.json.JSONObject;
//import sun.misc.BASE64Encoder;

/**
 *
 * @author pramodchaudhari
 */
//@WebServlet(name = "FaceRecogntionConnector", urlPatterns = {"/FaceRecogntionConnector"})
public class FaceRecogntionConnector extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        ((HttpServletResponse) response).setHeader("Access-Control-Allow-Origin", "*");
        PrintWriter out = response.getWriter();
        try {
            JSONObject json = new JSONObject();
            String methodName = request.getParameter("methodname");
            String userImage = request.getParameter("userimage");
            String docImage = request.getParameter("userdocimage");
            String userIdentiy = request.getParameter("useridentity");
            String phone=request.getParameter("phone");
            String email=request.getParameter("email");
            String faceorDocs="1"; 
//            String uploadDoc = (String)request.getSession().getAttribute("_imageFilePath");
//            String documentImage = null;
            if (methodName == null || userIdentiy == null) {

                json.put("_message", "Insufficent params");
                json.put("_result", "error");
                out.print(json);
                return;

            }            
            if (methodName.equals("enroll")) {
                if (userImage == null || docImage == null) {
                    json.put("_message", "Insufficent params");
                    json.put("_result", "error");
                    out.print(json);
                    return;
                }
            }
            if (methodName.equals("validate")) {
                if (userImage == null) {
                    json.put("_result", "error");
                    json.put("_message", "Insufficent params");
                    out.print(json);
                    return;
                }
            }
//            if(uploadDoc != null){               
//                byte[] imageBytes = convertFileToByteArray(uploadDoc);
////                String base64 = Base64.getEncoder().encodeToString(imageBytes);
////                BASE64Encoder encoder = new BASE64Encoder();
//                documentImage = new String(Base64.encode(imageBytes));
//            }
            FaceRecogntionWrapper fw = new FaceRecogntionWrapper();
            String sep = System.getProperty("file.separator");
            String usrhome = System.getProperty("catalina.home");
            if (usrhome == null) {
                usrhome = System.getenv("catalina.home");
            }
            usrhome += sep + "api-settings";
            String g_strPath = usrhome + sep;
            String filepath = usrhome + sep + "FaceRec.conf";
            fw.LoadAPConnector(filepath);
            List faceImages = new ArrayList();
            BfResponse bgResp = null;                        
            if (methodName.equals("checkuser")) {
                FaceRecogntionWrapper faceWrpapper = new FaceRecogntionWrapper();
                BfResponse bf = faceWrpapper.getUserStatus(userIdentiy);
                if (bf.errormessage.contains("User is enrolled for face recognition")) {
                    json.put("_message", "User Already Enrolled");
                    json.put("_result", "Success");
                    out.print(json);
                    return;

                }
                if (bf.errormessage.contains("User is not enrolled for face recognition")) {
                    json.put("_message", "User is not registered user");
                    json.put("_result", "error");
                    out.print(json);
                    return;

                }

            }
            
            if (methodName.equals("enroll")) {
                if (userImage.contains(",")) {
                    String[] imageArray= userImage.split(",");
                    for(int i=0;i<imageArray.length;i++){
                     faceImages.add(imageArray[i]);
                    }
                    
                } else {
                    faceImages.add(userImage);
                }
                if (!docImage.equals("")) {
                    faceImages.add(docImage.split(",")[1]);
                }
                    bgResp = fw.enrollandTrainFace(userIdentiy, "PASSPORT",phone,email, faceImages);
                    if(bgResp.errorcode==-1){
                        json.put("_result", "error");
                        json.put("_message", "Enrollment failed, Please try again");
                        out.print(json);
                        return;
                    }
                   if(bgResp.errorcode==-2)
                   {
                    json.put("_result", "error");
                    json.put("_message", bgResp.errormessage);
                    out.print(json);
                    return;
                   }
                   if(bgResp.errorcode==-5)
                   {
                    json.put("_result", "error");
                    json.put("_message", bgResp.errormessage +" Please try again");
                    out.print(json);
                    return;
                   }
                   if (bgResp.errormessage.contains("enrolled successfully")) {
                    request.getSession().setAttribute("userIdentity", userIdentiy);
                    json.put("_result", "Success");
                    json.put("_message", "User enrolled Successfully");
                    out.print(json);
                    return;
                }
            }
            if (methodName.equals("validate")) {
              
              int faceorDoc=Integer.parseInt(faceorDocs);
//              if(documentImage != null && uploadDoc != null){
//                  bgResp = fw.validateFace(userIdentiy, "PASSPORT", documentImage,faceorDoc);
//              }else{
                bgResp = fw.validateFace(userIdentiy, "PASSPORT", userImage.split(",")[1],faceorDoc);
              //}
               if(bgResp!=null){
             JSONObject resultJson= new  JSONObject(bgResp.errormessage);
             String  taskResult=resultJson.getString("task");
            org.json.JSONObject jsopn1= new org.json.JSONObject(taskResult);
                 String finalReult=  jsopn1.getString("Result");
                 if(finalReult!=null)
                 {
                     if(finalReult.contains("Image Verified")){
                     json.put("_result", "Success");
                     json.put("_message", finalReult);
                     out.print(json);
                     return;
                     }else if(finalReult.equals("image not Verified"))
                             {
                       json.put("_result", "error");
                       json.put("_message", "System can not match Your Face and Face from Document..!!");
                       out.print(json);
                       return;
                                 
                             }else{
                         
                                     
                                     }
                 
                   } else {
                       json.put("_result", "error");
                       json.put("_message", "Please try again!!!");
                       out.print(json);
                       return;
                   }                       
               }               
            }
            
            if(methodName.equals("facerec"))
            {
               
                bgResp = fw.recognizeFace(userImage.split(",")[1]);
                if (bgResp != null) {

                    if (bgResp.getErrorcode() == 0) {
                        json.put("_result", "Success");
                        try {
                            JSONObject jsonResRep = new JSONObject(bgResp.getData());
                            json.put("_nameSpan", jsonResRep.get("username").toString());
                            json.put("_mobileSpan", jsonResRep.get("phonno").toString());
                            json.put("_emailSpan", jsonResRep.get("emailid").toString());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        out.print(json);
                        return;

                    }else
                    {
                    json.put("_result", "error");
                    json.put("_message", "Please try again!!!");
                    out.print(json);
                    return;
                    
                    }
                } else {
                    json.put("_result", "error");
                    json.put("_message", "Please try again!!!");
                    out.print(json);
                    return;
                }                       
               }

        } catch (Exception ex) {
            ex.printStackTrace();
            
        }
    }
    
    public byte[] convertFileToByteArray(String filePath) {

        Path path = Paths.get(filePath);

        byte[] codedFile = null;

        try {
            codedFile = Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return codedFile;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
