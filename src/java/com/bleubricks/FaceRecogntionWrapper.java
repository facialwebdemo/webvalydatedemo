/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bleubricks;

import com.bleubricks.AllTrustManager;
import com.bleubricks.AllVerifier;
import com.bleubricks.LoadSettings;
import java.net.URL;
import java.util.Properties;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

/**
 *
 * @author pramodchaudhari
 */
public class FaceRecogntionWrapper {

    private static SSLContext sslContext = null;
    com.bleubricks.BFaceRecognitionServiceInterfaceImplService service;
    static com.bleubricks.BFaceRecognitionServiceInterface portReco;

    public int LoadAPConnector(String filepath) {
        String wsdlURL = "";
        try {
            LoadSettings.LoadFilePath(filepath);
            String wsdlName = LoadSettings.g_sSettings.getProperty("wsdl.name");
            String strSecure = LoadSettings.g_sSettings.getProperty("secured");
            String ip = LoadSettings.g_sSettings.getProperty("ip");
            String port = LoadSettings.g_sSettings.getProperty("port");
            wsdlURL = "http://" + ip + ":" + port + wsdlName;
            if (strSecure != null && strSecure.compareToIgnoreCase("yes") == 0) {
                wsdlURL = "https://" + ip + ":" + port + wsdlName;
            }
            if (strSecure != null && strSecure.equalsIgnoreCase("yes") == true) {
                try {
                    HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
                    sslContext = SSLContext.getInstance("TLS");
                    sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
                    HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

                } catch (Exception ex) {

                    return -5;
                }
                URL u = new URL(wsdlURL);
                if (portReco == null) {
                    service = new BFaceRecognitionServiceInterfaceImplService(u);
                    portReco = service.getBFaceRecognitionServiceInterfaceImplPort();
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    public BfResponse enrollandTrainFace(java.lang.String userIdentity, java.lang.String identityType,String phone,String email ,java.util.List<java.lang.String> faceImagesUrl) throws BFException {
        if (portReco == null) {
            com.bleubricks.BFaceRecognitionServiceInterfaceImplService service = new com.bleubricks.BFaceRecognitionServiceInterfaceImplService();
            com.bleubricks.BFaceRecognitionServiceInterface port = service.getBFaceRecognitionServiceInterfaceImplPort();
            return port.enrollandTrainFace(userIdentity, identityType,phone,email, faceImagesUrl);
        } else {
            return portReco.enrollandTrainFace(userIdentity, identityType,phone,email, faceImagesUrl);

        }
    }
    public BfResponse validateFace(java.lang.String userIdentity, java.lang.String identityType, java.lang.String faceToValidateUrl,int faceorDoc) throws BFException {
        if (portReco == null) {
            com.bleubricks.BFaceRecognitionServiceInterfaceImplService service = new com.bleubricks.BFaceRecognitionServiceInterfaceImplService();
            com.bleubricks.BFaceRecognitionServiceInterface port = service.getBFaceRecognitionServiceInterfaceImplPort();
            return port.validateFace(userIdentity, identityType, faceToValidateUrl,faceorDoc);
        } else {
            return portReco.validateFace(userIdentity, identityType, faceToValidateUrl,faceorDoc);
        }
    }

    public BfResponse recognizeFace(java.lang.String base64ImageSource) {
        if (portReco == null) {
            com.bleubricks.BFaceRecognitionServiceInterfaceImplService service = new com.bleubricks.BFaceRecognitionServiceInterfaceImplService();
            com.bleubricks.BFaceRecognitionServiceInterface port = service.getBFaceRecognitionServiceInterfaceImplPort();
            return port.recognizeFace(base64ImageSource);
        } else {
            return portReco.recognizeFace(base64ImageSource);
        }
    }
    
    public BfResponse deleteFace(java.lang.String userIdentity) {
        if (portReco == null) {
            com.bleubricks.BFaceRecognitionServiceInterfaceImplService service = new com.bleubricks.BFaceRecognitionServiceInterfaceImplService();
            com.bleubricks.BFaceRecognitionServiceInterface port = service.getBFaceRecognitionServiceInterfaceImplPort();
            return port.deleteFace(userIdentity);
        } else {
            return portReco.deleteFace(userIdentity);

        }
    }

    public BfResponse getUserStatus(java.lang.String userIdentity) {
        if (portReco == null) {
            com.bleubricks.BFaceRecognitionServiceInterfaceImplService service = new com.bleubricks.BFaceRecognitionServiceInterfaceImplService();
            com.bleubricks.BFaceRecognitionServiceInterface port = service.getBFaceRecognitionServiceInterfaceImplPort();
            return port.getUserStatus(userIdentity);
        } else {
            return portReco.getUserStatus(userIdentity);
        }
    }

//    private BfResponse enrollandTrainFace(java.lang.String userIdentity, java.lang.String identityType, java.util.List<java.lang.String> faceImagesUrl) throws BFException {
//        if (portReco == null) {
//            com.bleubricks.BFaceRecognitionServiceInterfaceImplService service = new com.bleubricks.BFaceRecognitionServiceInterfaceImplService();
//            com.bleubricks.BFaceRecognitionServiceInterface port = service.getBFaceRecognitionServiceInterfaceImplPort();
//            return port.enrollandTrainFace(userIdentity, identityType, faceImagesUrl);
//        } else {
//            return portReco.enrollandTrainFace(userIdentity, identityType, faceImagesUrl);
//
//        }
//    }

  

}
