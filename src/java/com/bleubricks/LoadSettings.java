package com.bleubricks;

import java.util.Properties;

public class LoadSettings {

    public static Properties g_sSettings = null;

    public static int LoadFilePath(String filepath) {
        if ( filepath == null || filepath.isEmpty() == true)
            return -1;
        
        PropsFileUtil p1 = new PropsFileUtil();

        if (p1.LoadFile(filepath) == true) {
            g_sSettings = p1.properties;
            return 0;
        } else {
            return -2;
        }
    }

}
