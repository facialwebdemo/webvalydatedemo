<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Facial Biometric</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />

<!--    <link rel="stylesheet" href="style.css"/>-->
    <script>
      var dataURL="";
      var useridentity="suresh077";
      var Imx="";
      var Imy="";
      var Imz="";
  function strcmpser(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
</script>
<%
    
    String userId = (String)session.getAttribute("userIdentity");
%>
</head>
<body class="blank">
    
<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><p><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="color-line"></div>

<div class="register-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>Register with your Document</h3>
                <small>register yourself with your documents </small>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                    <form action="#" id="loginForm">                        
                        <div id="demoScreen">
                            <div class="row col-lg-12" style="text-align: center; margin-left: 90px">
                                <div class="col-lg-12">
                                    <div class="hpanel hblue contact-panel" style="text-align: center">
                                        <div class="panel-body" style="height: 350px !important;width: 450px !important">
                                            <img alt="logo" class="img-circle m-b" src="images/a3.jpg" style="width: 200px !important; height: 200px !important">
                                            <p>
                                                Before Activate a camera be set in a bright light area and stay steady & focus on camera.
                                            </p>
                                        </div>
                                        <div class="panel-footer contact-footer" style="width: 450px !important">
                                            <div class="row">
                                                <a href="#" class="" onclick="App.start('glasses');">Activate camera</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                                
                        </div>                   
                        <div id="photoscreen" style="display: none">
                            <div class="row col-lg-12">
                                <div class="col-lg-12">
                                    <div class="hpanel hblue contact-panel" style="text-align: center;margin-left: 90px">
                                        <div class="panel-body" style="height: 350px !important;width: 450px !important">
                                            <video height="426" width="640" controls muted style="width:80px; height:75px; display: none"></video>
                                            <canvas id="output"  height="320" width="410" ></canvas>
                                            <canvas id="myCanvas"  ></canvas>
                                            <input type="hidden" value="<%=userId%>" id="txtuserid" class="form-control" name="txtuserid">
                                            <div class="colours" style="display:none;">
                                                <div id="red">
                                                    <input type="range" min=0 max=255 value=190 class="min">
                                                    <input type="range" min=0 max=255 value=240 class="max">
                                                </div>
                                                <div id="green">
                                                    <input type="range" min=0 max=255 value=0 class="min">
                                                    <input type="range" min=0 max=255 value=120 class="max">
                                                </div>
                                                <div id="blue">
                                                    <input type="range" min=0 max=255 value=90 class="min">
                                                    <input type="range" min=0 max=255 value=190 class="max">
                                                </div>
                                                <input type="hidden" id="myImageData"/>
                                            </div>
                                        </div>
                                        <p> </p>
                                    </div>
                                </div>
                            </div>                                                                                                                   
                        </div>
                        
                        <div id="userDetailsDiv" style="display: none;margin-left: 15px">
                                    <div class="row col-lg-12">
                                        <div class="col-lg-12">
                                            <div class="hpanel">
                                    <div class="panel-heading hbuilt">
                                        <div class="panel-tools">
                                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                            <a class="closebox"><i class="fa fa-times"></i></a>
                                        </div>
                                        <b>Your Details</b>
                                    </div>
                                    <div class="panel-body no-padding">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                
                                                <b>Name</b>
                                                <span style="margin-left: 335px" id="nameSpan">NA</span>
                                            </li>
                                            <li class="list-group-item ">
                                                
                                                <b>Mobile</b>
                                                <span style="margin-left: 330px" id="mobileSpan">NA</span>
                                            </li>
                                            <li class="list-group-item">
                                                
                                                <b>Email</b>
                                                <span style="margin-left: 340px" id="emailSpan">NA</span>
                                            </li>                                    
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                                        
                        </div>
                    </form>                                    
<!--                    <div class="row text-center">
                        <button class="btn btn-success">Register</button>
                        <button class="btn btn-default">Cancel</button>
                    </div>-->                                
        </div>
                <div id="capturePhoto" style="display: none">
                    <div class="m">
                        Capturing Document
                        <div class="progress m-t-xs full progress-striped active">
                            <div style="width: 110%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" role="progressbar" class=" progress-bar progress-bar-success">                            
                            </div>
                        </div>
                    </div>
                </div>
    </div>
    <div class="back-link">
        <a href="index.html" class="btn btn-primary">Back to Home</a>
    </div>        
    <div class="row">
        <div class="col-md-12 text-center">
            <strong>Facial</strong> - Biometric WebApp <br/> 2017 Copyright BlueBricks Pty. Ltd.
        </div>
    </div>
</div>

<!-- Vendor scripts -->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>

<!-- App scripts -->
<script src="scripts/homer.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="scripts/ccv.js"></script>
<script type="text/javascript" src="scripts/face.js"></script>
<script src="scripts/registerDoc.js" type="text/javascript"></script>
<script type="text/javascript" src="scripts/stackblur.js"></script>


</body>
</html>