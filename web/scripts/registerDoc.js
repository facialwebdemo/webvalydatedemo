/** Start Google Chrome Canary with open -a Google\ Chrome\ Canary --args --enable-media-stream  OR enable the flag in about:flags **/
  var x=3;
 var docx=4;
 var faceInstruction=3;
  var myImageStored1="";
var captureImageDiv="";
var App = {            
	// Run if we do have camera support
	successCallback : function(stream) {
        console.log('yeah! camera support!');
        if(window.webkitURL) {
            App.video.src = window.webkitURL ? window.webkitURL.createObjectURL(stream) : stream;
        }
        else {
            App.video.src = stream;
        }
    },
	// run if we dont have camera support
	errorCallback : function(error) {
		alert('An error occurred while trying to get camera access (Your browser probably doesnt support getUserMedia() ): ' + error.code);
		return;
	},
	drawToCanvas : function(effect) {
		var video = App.video,
			ctx = App.ctx,
			canvas = App.canvas,
			i;

			//ctx.drawImage(video, 0, 0, 520,426);
               ctx.drawImage(video, 0, 0, 520,426);
			App.pixels = ctx.getImageData(0,0,canvas.width,canvas.height);

		// Hipstergram!
		
		if (effect === 'hipster') {
			for (i = 0; i < App.pixels.data.length; i=i+4) {
				App.pixels.data[i + 0] = App.pixels.data[i + 0] * 3 ;
				App.pixels.data[i + 1] = App.pixels.data[i + 1] * 2;
				App.pixels.data[i + 2] = App.pixels.data[i + 2] - 10;
			}
			ctx.putImageData(App.pixels,0,0);
		}

		// Blur!

		else if (effect === 'blur') {
			stackBlurCanvasRGBA('output',0,0,515,426,20);
		}

		// Green Screen

		else if (effect === 'greenscreen') {
				
					/* Selectors */
					var rmin = $('#red input.min').val();
					var gmin = $('#green input.min').val();
					var bmin = $('#blue input.min').val();
					var rmax = $('#red input.max').val();
					var gmax = $('#green input.max').val();
					var bmax = $('#blue input.max').val();

					// console.log(rmin,gmin,bmin,rmax,gmax,bmax);
					
					for (i = 0; i < App.pixels.data.length; i=i+4) {
									red = App.pixels.data[i + 0];
									green = App.pixels.data[i + 1];
									blue = App.pixels.data[i + 2];
									alpha = App.pixels.data[i + 3];

									if (red >= rmin && green >= gmin && blue >= bmin && red <= rmax && green <= gmax && blue <= bmax ) {
										App.pixels.data[i + 3] = 0;
									}
					}

					ctx.putImageData(App.pixels,0,0);

		}
               
        else if (effect === 'glasses') {
           if(faceInstruction==3)
           {
               //alert("Please show your face in Camera");
//               swal({
//                    title: "Camera Activated!",
//                    text: "Please show your document image, You need to be set in bright area & look steady at camera!!",
//                    type: "success"
//                });
               faceInstruction=faceInstruction+1;
               if(captureImageDiv.style.display === 'none'){
                   captureImageDiv.style.display = 'block';
               }
           }
            var comp = ccv.detect_objects({"canvas": (App.canvas),
                "cascade": cascade,
                "interval": 10,
                "min_neighbors": 1});
          
                for (i = 0; i < comp.length; i++) {
             //   ctx.drawImage(App.glasses, comp[i].x+2, comp[i].y+2, comp[i].width+2, comp[i].height+5);
        ctx.drawImage(App.glasses, comp[i].x-30, comp[i].y-60, comp[i].width+40, comp[i].height+120);
                
//                var new_canvas = document.querySelector("#myCanvas");
//                new_canvas.width = 200;
//                new_canvas.height = 200;
//                alert(comp[i].width+10+ comp[i].height+10);
//                new_canvas.getContext('2d').drawImage(canvas, comp[i].width+10,  comp[i].height+10);
//                var img = new_canvas.toDataURL("image/png");
//                console.log("Image" + img);
                    
                    var delayMillis = 7000; //1 second
                   
               
                window.setTimeout(function () {
                    if (x > 0) {
                       

                            //alert(x);
//                            swal({
//                                title: "Verified!",
//                                text: "User Verified!",
//                                type: "success"
//                            });
                            Imx = canvas.toDataURL("image/png").split(",")[1];
                           // alert(Imx);
                        
                        if (x == 1)
                        {
                            //alert(x);
                            Imy = Imx + "," + canvas.toDataURL("image/png").split(",")[1];
                           // alert(Imy);
                           
                            var dataURLForDoc = canvas.toDataURL("image/png");
                            swal({
                                title: "Success!",
                                text: "Your Document image captured and sending for verification, Please wait",
                                type: "success"
                            });
                            var useridmageBase64 = dataURLForDoc;// dataURL;//document.getElementById("myImageData").innerHTML;
                            var userdocImage = "";//document.getElementById("userdocimage").value;
                            useridentity = document.getElementById("txtuserid").value;
                            //alert(useridentity);
                            var data = {"useridentity": useridentity, "userimage": useridmageBase64};
                            var s = './FaceRecogntionConnector?methodname=validate';
                            $.ajax({
                                type: 'POST',
                                url: s,
                                data: data,
                                async: false,
                                dataType: 'json',
                                success: function (data) {
                                    if (strcmpser(data._result, "error") == 0) {
                                      //alert(data._message);
                                      if (captureImageDiv.style.display === 'block') {
                                            captureImageDiv.style.display = 'none';
                                        }
                                        swal({
                                            title: "Error!",
                                            text: "Your Doument image is not recognize, Please try again ",
                                            type: "error"
                                        });
                                      window.setTimeout(function(){
                                          window.location.href="registerDoc.jsp";
                                      },3000);
                                    }
                                    if (strcmpser(data._result, "Success") == 0) {
                                        //$('#password-reset-result').html("<span><font color=blue>" + data.message + "</font></span>");
//                                        alert(data._message);
//                                        alert('Now Show your document image');
                                        if (captureImageDiv.style.display === 'block') {
                                            captureImageDiv.style.display = 'none';
                                        }
                                        swal({
                                            title: "Success!",
                                            text: "Your document image verified successfully, Please do verify to get your details",
                                            type: "success"
                                        });
//                                        document.getElementById("photoscreen").style.display="none";
//                                        document.getElementById("userDetailsDiv").style.display="block";
//                                        document.getElementById("nameSpan").innerHTML=data._nameSpan;
//                                        document.getElementById("mobileSpan").innerHTML=data._mobileSpan;
//                                        document.getElementById("emailSpan").innerHTML=data._emailSpan;
                                    window.setTimeout(function(){
                                        window.location.href="index.html";
                                    },3000);
                                    }
                                }
                            });
                        }
//                        if (x == 1)
//                        {
//                            alert(x);
//                            Imz = Imy + "," + canvas.toDataURL("image/png").split(",")[1];
//                           // alert(Imz);
//                        }
                         x = x - 1;
//                        if (strcmpser(dataURL, "") == 0)
//                        {
//                        
//                          Imx =canvas.toDataURL("image/png").split(",")[1];
//                        
////                           
//                        } else {
//                         
//                            
//                         
//                      // dataURL = dataURL +","+canvas.toDataURL("image/png").split(",")[1];
//                         //   alert("NextTime");
//                            //dataURL = dataURL.concat(","+canvas.toDataURL("image/png"));
//                          
//                        }
//                        if (x == 0)
//                        {
//                            alert("TO Send IMages to server");
                           
                        //}
                   
                    //} else {
//                        if(docx==3)
//                        {
//                          
//                        }
//                 window.setTimeout(function(){
//                        if (docx >0) {
//                           docx=docx-1;
//                            var dataURLForDoc = canvas.toDataURL("image/png");
//                            if (docx==0)
//                            {
//                                alert("Sending  Images for Verification");
//                                useridentity=document.getElementById("txtuserid").value;
//                                var useridmageBase64 = dataURLForDoc; //document.getElementById("userimage").value; 
//                                // var useridentity="";   //document.getElementById("useridentity").value;
//                                var data = {"useridentity": useridentity, "userimage": useridmageBase64};
//                                var s = './FaceRecogntionConnector?methodname=validate';
//                                $.ajax({
//                                    type: 'POST',
//                                    url: s,
//                                    data: data,
//                                    async:false,
//                                    dataType: 'json',
//                                    success: function (data) {
//                                        if (strcmpser(data._result, "error") == 0) {
////                $('#password-reset-result').html("<span><font color=red>" + data.message + "</font></span></small>");
//                                            alert(data._message);
//                                        }
//                                        if (strcmpser(data._result, "Success") == 0) {
//                                            //$('#password-reset-result').html("<span><font color=blue>" + data.message + "</font></span>");
//                                            window.location.href = 'LandingPage.html';
//                                        }
//                                    }
//                                });
//
//                            }
//                        }
//                        }, "6000");
//                       else{
//                         //code for verification  
//                       
//                        }
                    }
                  
                }, delayMillis);

            }
            

        }
					
	},

	start : function(effect) {
     
            if(App.playing) { clearInterval(App.playing); }
		App.playing = setInterval(function() {
			App.drawToCanvas(effect);
		},50);
           document.getElementById("photoscreen").style.display='block'; 
           document.getElementById("demoScreen").style.display='none';
           captureImageDiv=document.getElementById('capturePhoto');    
	}
};

    App.init = function() {
	// Prep the document
	App.video = document.querySelector('video');	
	App.glasses = new Image();
	App.glasses.src = "i/glasses.png";
        //App.glasses.src = "glasses.png";
	App.canvas = document.querySelector("#output");
	App.ctx = this.canvas.getContext("2d");
	// Finally Check if we can run this puppy and go!
	if (navigator.getUserMedia) {
            getUserMediaAccess();
		//navigator.getUserMedia('video', App.successCallback, App.errorCallback);
	}

	// App.start();

};


document.addEventListener("DOMContentLoaded", function() {
	console.log('ready!');
	App.init();
}, false);


/*! Navigator Getusermedia - v0.1.0 - 3/9/2012
* https://github.com/rwldrn/navigator.getusermedia
* Copyright (c) 2012 Rick Waldron <waldron.rick@gmail.com>; Licensed MIT */
//(function (a, b) {
//    a.unprefix || (a.URL || (a.URL = a.webkitURL || a.msURL || a.oURL), b.getUserMedia || (b.getUserMedia = b.webkitGetUserMedia || b.mozGetUserMedia || b.msGetUserMedia));
//    var c = !0, d = b.getUserMedia;
//    try {
//        b.getUserMedia({video: !0, audio: !0}, function () {})
//    } catch (e) {
//        c = !1
//    }
//    b.getUserMedia = function (e, f, g) {
//        var h, i, j = Object.keys(e), k = {video: 1, audio: 1};
//        if (!c)
//            i = j.filter(function (a) {
//                return this[a] && k[a]
//            }, e).join(",");
//        else {
//            i = {};
//            for (h in e)
//                i[h] = e[h] && k[h]
//        }
//        d.call(b, i, function (b) {
//            var c;
//            b.label && b.readyState === 1 && (c = a.URL.createObjectURL(b)), f(b, c)
//        }, g || function () {})
//    }
//})(typeof window == "object" && window || this, this.navigator || {})


//newy added code

function getUserMediaAccess()
{
    navigator.getUserMedia = navigator.getUserMedia ||
                         navigator.webkitGetUserMedia ||
                         navigator.mozGetUserMedia;

if (navigator.getUserMedia) {
   navigator.getUserMedia({ audio: true, video: { width: 1280, height: 720 } },
      function(stream) {
         var video = document.querySelector('video');
         video.srcObject = stream;
         video.onloadedmetadata = function(e) {
           video.play();
         };
      },
      function(err) {
         console.log("The following error occurred: " + err.name);
      }
   );
} else {
   console.log("getUserMedia not supported");
}
    
}

function registerUser(){
    swal({
        title: "Success!",
        text: "You have been successfully register!",
        type: "success"
    });
}